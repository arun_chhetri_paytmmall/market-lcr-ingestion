package com.paytmmall.pdp.ingest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class CatalogProductRecord implements Serializable {

  private Long id; //Id of the product
  private String name;
  private Long parent_id; //parent ID of the product
  private Long merchant_id;
  private String merchant_name;
  private Integer value;
  private Integer product_type;
  private BigDecimal mrp;
  private BigDecimal price;
  private Long brand_id;
  private String brand;
  private Integer visibility;
  private Long vertical_id;
  private String attributes;
  private Timestamp created_at;
  private Timestamp updated_at;
  private Integer is_in_stock;
  private Integer pay_type;
  private Integer conv_fee;
  private BigDecimal shipping_charge;
  private Integer bargain;
  private Integer discount;
  private Integer max_dispatch_time;
  private Integer need_shipping;
  private Integer fulfillment_mode;

  //Fields from category table
  private String category_name;
  private Integer category_level;
  private Long category_id;
  private Long category_parent_id;
  private String category_parent_name;
  private Integer category_parent_level;
  private Long category_parent_parent_id;
  private String category_parent_parent_name;
  private Integer category_parent_parent_level;
  private Long category_parent_parent_parent_id;
  private String category_parent_parent_parent_name;
  private Integer category_parent_parent_parent_level;
  private Long category_parent_parent_parent_parent_id;
  private String category_parent_parent_parent_parent_name;
  private Long category_parent_parent_parent_parent_parent_id;
  private Integer category_parent_parent_parent_parent_level;

  //Fields from inventory
  private Long inventory_qty;

  public CatalogProductRecord() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public Long getMerchant_id() {
    return merchant_id;
  }

  public void setMerchant_id(Long merchant_id) {
    this.merchant_id = merchant_id;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public Integer getProduct_type() {
    return product_type;
  }

  public void setProduct_type(Integer product_type) {
    this.product_type = product_type;
  }

  public BigDecimal getMrp() {
    return mrp;
  }

  public void setMrp(BigDecimal mrp) {
    this.mrp = mrp;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Long getBrand_id() {
    return brand_id;
  }

  public void setBrand_id(Long brand_id) {
    this.brand_id = brand_id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public Integer getVisibility() {
    return visibility;
  }

  public void setVisibility(Integer visibility) {
    this.visibility = visibility;
  }

  public Long getVertical_id() {
    return vertical_id;
  }

  public void setVertical_id(Long vertical_id) {
    this.vertical_id = vertical_id;
  }

  public String getAttributes() {
    return attributes;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public String getCategory_name() {
    return category_name;
  }

  public void setCategory_name(String category_name) {
    this.category_name = category_name;
  }

  public Long getCategory_id() {
    return category_id;
  }

  public void setCategory_id(Long category_id) {
    this.category_id = category_id;
  }

  public Integer getCategory_level() {
    return category_level;
  }

  public void setCategory_level(Integer category_level) {
    this.category_level = category_level;
  }

  public Long getCategory_parent_id() {
    return category_parent_id;
  }

  public void setCategory_parent_id(Long category_parent_id) {
    this.category_parent_id = category_parent_id;
  }

  public String getCategory_parent_name() {
    return category_parent_name;
  }

  public void setCategory_parent_name(String category_parent_name) {
    this.category_parent_name = category_parent_name;
  }

  public Integer getCategory_parent_level() {
    return category_parent_level;
  }

  public void setCategory_parent_level(Integer category_parent_level) {
    this.category_parent_level = category_parent_level;
  }

  public Long getCategory_parent_parent_id() {
    return category_parent_parent_id;
  }

  public void setCategory_parent_parent_id(Long category_parent_parent_id) {
    this.category_parent_parent_id = category_parent_parent_id;
  }

  public String getCategory_parent_parent_name() {
    return category_parent_parent_name;
  }

  public void setCategory_parent_parent_name(String category_parent_parent_name) {
    this.category_parent_parent_name = category_parent_parent_name;
  }

  public Integer getCategory_parent_parent_level() {
    return category_parent_parent_level;
  }

  public void setCategory_parent_parent_level(Integer category_parent_parent_level) {
    this.category_parent_parent_level = category_parent_parent_level;
  }

  public Long getCategory_parent_parent_parent_id() {
    return category_parent_parent_parent_id;
  }

  public void setCategory_parent_parent_parent_id(Long category_parent_parent_parent_id) {
    this.category_parent_parent_parent_id = category_parent_parent_parent_id;
  }

  public String getCategory_parent_parent_parent_name() {
    return category_parent_parent_parent_name;
  }

  public void setCategory_parent_parent_parent_name(String category_parent_parent_parent_name) {
    this.category_parent_parent_parent_name = category_parent_parent_parent_name;
  }

  public Integer getCategory_parent_parent_parent_level() {
    return category_parent_parent_parent_level;
  }

  public void setCategory_parent_parent_parent_level(Integer category_parent_parent_parent_level) {
    this.category_parent_parent_parent_level = category_parent_parent_parent_level;
  }

  public Long getCategory_parent_parent_parent_parent_id() {
    return category_parent_parent_parent_parent_id;
  }

  public void setCategory_parent_parent_parent_parent_id(
      Long category_parent_parent_parent_parent_id) {
    this.category_parent_parent_parent_parent_id = category_parent_parent_parent_parent_id;
  }

  public String getCategory_parent_parent_parent_parent_name() {
    return category_parent_parent_parent_parent_name;
  }

  public void setCategory_parent_parent_parent_parent_name(
      String category_parent_parent_parent_parent_name) {
    this.category_parent_parent_parent_parent_name = category_parent_parent_parent_parent_name;
  }

  public Long getCategory_parent_parent_parent_parent_parent_id() {
    return category_parent_parent_parent_parent_parent_id;
  }

  public void setCategory_parent_parent_parent_parent_parent_id(
      Long category_parent_parent_parent_parent_parent_id) {
    this.category_parent_parent_parent_parent_parent_id = category_parent_parent_parent_parent_parent_id;
  }

  public Integer getCategory_parent_parent_parent_parent_level() {
    return category_parent_parent_parent_parent_level;
  }

  public void setCategory_parent_parent_parent_parent_level(
      Integer category_parent_parent_parent_parent_level) {
    this.category_parent_parent_parent_parent_level = category_parent_parent_parent_parent_level;
  }

  public String getMerchant_name() {
    return merchant_name;
  }

  public void setMerchant_name(String merchant_name) {
    this.merchant_name = merchant_name;
  }

  public Timestamp getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Timestamp created_at) {
    this.created_at = created_at;
  }

  public Timestamp getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Timestamp updated_at) {
    this.updated_at = updated_at;
  }

  public Integer getIs_in_stock() {
    return is_in_stock;
  }

  public void setIs_in_stock(Integer is_in_stock) {
    this.is_in_stock = is_in_stock;
  }

  public Integer getPay_type() {
    return pay_type;
  }

  public void setPay_type(Integer pay_type) {
    this.pay_type = pay_type;
  }

  public Integer getConv_fee() {
    return conv_fee;
  }

  public void setConv_fee(Integer conv_fee) {
    this.conv_fee = conv_fee;
  }

  public BigDecimal getShipping_charge() {
    return shipping_charge;
  }

  public void setShipping_charge(BigDecimal shipping_charge) {
    this.shipping_charge = shipping_charge;
  }

  public Integer getBargain() {
    return bargain;
  }

  public void setBargain(Integer bargain) {
    this.bargain = bargain;
  }

  public Integer getDiscount() {
    return discount;
  }

  public void setDiscount(Integer discount) {
    this.discount = discount;
  }

  public Integer getMax_dispatch_time() {
    return max_dispatch_time;
  }

  public void setMax_dispatch_time(Integer max_dispatch_time) {
    this.max_dispatch_time = max_dispatch_time;
  }

  public Integer getNeed_shipping() {
    return need_shipping;
  }

  public void setNeed_shipping(Integer need_shipping) {
    this.need_shipping = need_shipping;
  }

  public Integer getFulfillment_mode() {
    return fulfillment_mode;
  }

  public void setFulfillment_mode(Integer fulfillment_mode) {
    this.fulfillment_mode = fulfillment_mode;
  }

  public Long getInventory_qty() {
    return inventory_qty;
  }

  public void setInventory_qty(Long inventory_qty) {
    this.inventory_qty = inventory_qty;
  }

  @Override
  public String toString() {
    return "CatalogProductRecord{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", parent_id=" + parent_id +
        ", merchant_id=" + merchant_id +
        ", merchant_name='" + merchant_name + '\'' +
        ", value=" + value +
        ", product_type=" + product_type +
        ", mrp=" + mrp +
        ", price=" + price +
        ", brand_id=" + brand_id +
        ", brand='" + brand + '\'' +
        ", visibility=" + visibility +
        ", vertical_id=" + vertical_id +
        ", attributes='" + attributes + '\'' +
        ", created_at=" + created_at +
        ", updated_at=" + updated_at +
        ", is_in_stock=" + is_in_stock +
        ", pay_type=" + pay_type +
        ", conv_fee=" + conv_fee +
        ", shipping_charge=" + shipping_charge +
        ", bargain=" + bargain +
        ", discount=" + discount +
        ", max_dispatch_time=" + max_dispatch_time +
        ", need_shipping=" + need_shipping +
        ", fulfillment_mode=" + fulfillment_mode +
        ", category_name='" + category_name + '\'' +
        ", category_level=" + category_level +
        ", category_id=" + category_id +
        ", category_parent_id=" + category_parent_id +
        ", category_parent_name='" + category_parent_name + '\'' +
        ", category_parent_level=" + category_parent_level +
        ", category_parent_parent_id=" + category_parent_parent_id +
        ", category_parent_parent_name='" + category_parent_parent_name + '\'' +
        ", category_parent_parent_level=" + category_parent_parent_level +
        ", category_parent_parent_parent_id=" + category_parent_parent_parent_id +
        ", category_parent_parent_parent_name='" + category_parent_parent_parent_name + '\'' +
        ", category_parent_parent_parent_level=" + category_parent_parent_parent_level +
        ", category_parent_parent_parent_parent_id=" + category_parent_parent_parent_parent_id +
        ", category_parent_parent_parent_parent_name='" + category_parent_parent_parent_parent_name
        + '\'' +
        ", category_parent_parent_parent_parent_parent_id="
        + category_parent_parent_parent_parent_parent_id +
        ", category_parent_parent_parent_parent_level=" + category_parent_parent_parent_parent_level
        +
        ", inventory_qty=" + inventory_qty +
        '}';
  }
}
