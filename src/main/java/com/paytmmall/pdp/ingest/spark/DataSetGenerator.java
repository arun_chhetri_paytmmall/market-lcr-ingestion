package com.paytmmall.pdp.ingest.spark;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytmmall.pdp.config.Config;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.collection.JavaConversions;

public class DataSetGenerator {

  private static Logger LOGGER = Logger.getLogger(DataSetGenerator.class);

  private static ObjectMapper objectMapper = new ObjectMapper();

  public static Dataset<Row> decorateCatalogProductSparkSession(final SparkSession sparkSession)
      throws IOException {
    switch (Config.getEnvironmentConfig().getCatalogProductSourceType()) {
      case "jdbc": {
        final Properties dbProperties = new Properties();
        dbProperties.put("user", Config.getEnvironmentConfig().getCatalogProductjdbcUser());
        dbProperties
            .put("password", Config.getEnvironmentConfig().getCatalogProductjdbcPassword());
        dbProperties.put("driver", "com.mysql.jdbc.Driver");
        return sparkSession.read().option("inferSchema", "true")
            .jdbc(Config.getEnvironmentConfig().getCatalogProductSourceTypeURL(),
                "catalog_product", dbProperties);
      }
      case "s3": {
        return sparkSession.read().parquet(getS3UrlForMktPlaceProduct());
      }
      case "file": {
        return sparkSession.read().parquet(Config.getEnvironmentConfig().getCatalogProductSourceTypeURL());
      }
      default:
        throw new RuntimeException("Not suitable datasource provider for catalog product");
    }
  }

  public static Dataset<Row> decorateCatalogCategorySparkSession(final SparkSession sparkSession)
      throws IOException {
    switch (Config.getEnvironmentConfig().getCatalogCategorySourceType()) {
      case "jdbc": {
        final Properties dbProperties = new Properties();
        dbProperties.put("user", Config.getEnvironmentConfig().getCatalogCategoryjdbcUser());
        dbProperties
            .put("password", Config.getEnvironmentConfig().getCatalogCategoryjdbcPassword());
        dbProperties.put("driver", "com.mysql.jdbc.Driver");
        return sparkSession.read().option("inferSchema", "true")
            .jdbc(Config.getEnvironmentConfig().getCatalogCategorySourceTypeURL(),
                "catalog_category", dbProperties);
      }
      case "s3": {
        return sparkSession.read().parquet(getS3URLForMktPlaceCatalogCategory());
      }
      case "file": {
        return sparkSession.read().parquet(Config.getEnvironmentConfig().getCatalogCategorySourceTypeURL());
      }
      default:
        throw new RuntimeException("Not suitable datasource provider for catalog category");
    }
  }

  public static Dataset<Row> decorateInventorySparkSession(final SparkSession sparkSession)
      throws IOException {
    switch (Config.getEnvironmentConfig().getInventorySourceType()) {
      case "jdbc": {
        final Properties dbProperties = new Properties();
        dbProperties.put("user", Config.getEnvironmentConfig().getInventoryjdbcUser());
        dbProperties
            .put("password", Config.getEnvironmentConfig().getInventoryjdbcPassword());
        dbProperties.put("driver", "com.mysql.jdbc.Driver");
        return sparkSession.read().option("inferSchema", "true")
            .jdbc(Config.getEnvironmentConfig().getInventorySourceTypeURL(),
                "inventory", dbProperties);
      }
      case "s3": {
        return sparkSession.read().parquet(getS3URLForMktPlaceInventory());
      }
      case "file": {
        return sparkSession.read().parquet(Config.getEnvironmentConfig().getInventorySourceTypeURL());
      }
      default:
        throw new RuntimeException("Not suitable datasource provider for inventory");
    }
  }


  public static scala.collection.Seq<String> getS3UrlForMktPlaceProduct() throws IOException {
    return JavaConversions.asScalaBuffer(getPartitionsFromS3("31")).toSeq();
  }

  public static scala.collection.Seq<String> getS3URLForMktPlaceCatalogCategory()
      throws IOException {
    return JavaConversions.asScalaBuffer(getPartitionsFromS3("30")).toSeq();
  }

  public static scala.collection.Seq<String> getS3URLForMktPlaceInventory() throws IOException {
    return JavaConversions.asScalaBuffer(getPartitionsFromS3("583")).toSeq();
  }

  public static List<String> getPartitionsFromS3(String apiNumber) throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      String url = String.format(Config.getEnvironmentConfig().getDatasetApiEndpoint(), apiNumber);
      LOGGER.info(String.format("Dataset API %s url = %s", apiNumber, url));
      HttpGet httpGet = new HttpGet(
          url
      );
      try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
        List<String> partitionSet = new ArrayList<>();
        Map<String, Object> responseJson = objectMapper
            .readValue(response.getEntity().getContent(), Map.class);
        Map<String, String> partitions = (Map<String, String>) responseJson.get("partitions");
        for (Map.Entry<String, String> partition : partitions.entrySet()) {
          partitionSet.add(partition.getValue().replaceFirst("s3:", "s3a:") + "/*.parquet");
        }
        LOGGER.info("Partition set for api number = " + apiNumber + "\n" + partitionSet);
        return partitionSet;
      }
    }
  }
}
