package com.paytmmall.pdp.ingest.spark;

import com.paytmmall.pdp.config.Config;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

public class SparkSessionFactory {

  private static SparkSession sparkSession;

  public static SparkSession getSparkSession() {
    if (sparkSession == null) {
      synchronized (SparkSessionFactory.class) {
        if (sparkSession == null) {
          final SparkConf conf = new SparkConf()
              .setAppName(Config.getEnvironmentConfig().getApplicationName())
              .setMaster(Config.getEnvironmentConfig().getSparkMasterURL());
          sparkSession = SparkSession.builder()
              .appName(Config.getEnvironmentConfig().getApplicationName()).config(conf)
              .getOrCreate();
          if (Config.getEnvironmentConfig().isAwsEnvironment()) {
            System.setProperty("com.amazonaws.services.s3.enableV4", "true");
            sparkSession.sparkContext().hadoopConfiguration().set("fs.s3a.endpoint", "s3.ap-south-1.amazonaws.com");
            sparkSession.sparkContext().hadoopConfiguration().set("com.amazonaws.services.s3.enableV4", "true");
            sparkSession.sparkContext().hadoopConfiguration().set("fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem");
            sparkSession.sparkContext().hadoopConfiguration().set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.InstanceProfileCredentialsProvider");
          }
        }
      }
    }
    return sparkSession;
  }
}
