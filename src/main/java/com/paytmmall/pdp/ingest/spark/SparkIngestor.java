package com.paytmmall.pdp.ingest.spark;

import com.paytmmall.pdp.consumer.BatchResponse;
import com.paytmmall.pdp.consumer.BatchResponse.PResponse;
import com.paytmmall.pdp.consumer.ConsumerFactory;
import com.paytmmall.pdp.ingest.CatalogProductRecord;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.ForeachPartitionFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;

public class SparkIngestor {

  private static Logger LOGGER = Logger.getLogger(SparkIngestor.class);

  public static void ingest() throws Exception {
    //TODO: Read old RDD which might be saved somewhere
    //IF old rdd is not null, call delta Ingest
    long startTime = System.currentTimeMillis();
    String oldPath = DataUtil.getOldDataPath();
    boolean allIngest = oldPath == null ? true : false;
    try {
      try (SparkSession sparkSession = SparkSessionFactory.getSparkSession()) {
        if (allIngest) {
          LOGGER.info("Starting all data ingestion");
          allIngest(sparkSession);
        } else {
          LOGGER.info("Starting delta data ingestion. Old saved rdd path = " + oldPath);
          deltaIngest(sparkSession, oldPath);
        }
      }
    } finally {
      ConsumerFactory.getConsumer().close();
      LOGGER.info("Finished all ingestion of data. Time delta = " + Long
          .toString(System.currentTimeMillis() - startTime) + " (ms)");
    }
  }

  private static void allIngest(final SparkSession sparkSession) throws IOException {
    Dataset<CatalogProductRecord> resultJoin = getAllCPRDataSet(sparkSession);
    LOGGER.info("Total number of products after join = " + resultJoin.count());
    dumpCatalogProductDataSet(resultJoin);
    //Save RDD on disk for Delta
    saveRddOnDisk(resultJoin);
    LOGGER.info(String.format("Data ingestion successful done for %d records", resultJoin.count()));
  }

  private static void deltaIngest(final SparkSession sparkSession,
      final String pathToOldRdd)
      throws IOException {
    Dataset<CatalogProductRecord> oldCatalogProductRdd = sparkSession.read().parquet(pathToOldRdd)
        .as(Encoders.bean(CatalogProductRecord.class)).persist(StorageLevel.MEMORY_AND_DISK());
    //Get current all dataview
    Dataset<CatalogProductRecord> allNewDatasetFromDAAS = getAllCPRDataSet(sparkSession);
    Dataset<CatalogProductRecord> deleteDiff = oldCatalogProductRdd.except(allNewDatasetFromDAAS);
    List<Boolean> deleteResult = deleteCatalogProductDataSetFromConsumer(deleteDiff)
        .collectAsList();
    LOGGER.info("Deleted cpr records from elastic search = " + deleteResult.size());
    Dataset<CatalogProductRecord> updateDiff = allNewDatasetFromDAAS.except(oldCatalogProductRdd);
    List<Boolean> result = writeCatalogProductDataSetInConsumer(updateDiff).collectAsList();
    LOGGER.info("Inserted cpr records into elastic search = " + deleteResult.size());
    //Save the new dataset on disk
    saveRddOnDisk(allNewDatasetFromDAAS);
  }

  private static Dataset<CatalogProductRecord> getAllCPRDataSet(final SparkSession sparkSession)
      throws IOException {
    Dataset<Row> catalogProductRecordDataset = SparkUtil.generateCatalogProductView(sparkSession)
        .persist(
            StorageLevel.MEMORY_AND_DISK());
    long catalogProductRecordDatasetCount = catalogProductRecordDataset.count();
    LOGGER.info("Total number of products in dataset = " + catalogProductRecordDatasetCount);
    Dataset<Row> categoryFlatView = SparkUtil.generateCategoryView(sparkSession)
        .persist(StorageLevel.MEMORY_AND_DISK());
    LOGGER.info("Total number of categories in dataset = " + categoryFlatView.count());
    Dataset<Row> productInventoryView = SparkUtil.generateInventoryView(sparkSession)
        .persist(StorageLevel.MEMORY_AND_DISK());
    LOGGER.info("Total number of product inventory in dataset = " + productInventoryView.count());
    Dataset<CatalogProductRecord> resultJoin = SparkUtil
        .joinCatalogProductAndInventoryAndCategory(catalogProductRecordDataset,
            productInventoryView,
            categoryFlatView).as(
            Encoders.bean(CatalogProductRecord.class)).persist(StorageLevel.MEMORY_AND_DISK());
    return resultJoin;
  }

  private static void dumpCatalogProductDataSet(Dataset<CatalogProductRecord> cprDataSet) {
    cprDataSet.foreachPartition(new ForeachPartitionFunction<CatalogProductRecord>() {
      @Override
      public void call(Iterator<CatalogProductRecord> cprIterator) throws Exception {
        BatchResponse batchResponse = ConsumerFactory.getConsumer().batchConsume(cprIterator);
        for (PResponse pResponse : batchResponse.getAll()) {
          if (! pResponse.getSuccess()) {
            LOGGER.error(String.format("Failed insertion of record %s due to %s", pResponse.getId(),
                pResponse.getError()));
          }
        }
      }
    });
  }

  private static Dataset<Boolean> writeCatalogProductDataSetInConsumer(
      Dataset<CatalogProductRecord> cprDataSet) {
    return cprDataSet
        .map(new MapFunction<CatalogProductRecord, Boolean>() {
          @Override
          public Boolean call(final CatalogProductRecord catalogProductRecord) {
            try {
              ConsumerFactory.getConsumer().consume(catalogProductRecord);
            } catch (Exception ex) {
              LOGGER.error(
                  "Dropping catalog product record due to error. Catalog product record = "
                      + catalogProductRecord.toString(), ex);
              return Boolean.FALSE;
            }
            return Boolean.TRUE;
          }
        }, Encoders.BOOLEAN());
  }

  private static Dataset<Boolean> deleteCatalogProductDataSetFromConsumer(
      Dataset<CatalogProductRecord> cprDataSet) {
    return cprDataSet
        .map(new MapFunction<CatalogProductRecord, Boolean>() {
          @Override
          public Boolean call(final CatalogProductRecord catalogProductRecord) {
            try {
              ConsumerFactory.getConsumer().delete(catalogProductRecord);
            } catch (Exception ex) {
              LOGGER.error(
                  "Dropping catalog product record (Delete) due to error. Catalog product record = "
                      + catalogProductRecord.toString(), ex);
              return Boolean.FALSE;
            }
            return Boolean.TRUE;
          }
        }, Encoders.BOOLEAN());
  }


  private static void saveRddOnDisk(Dataset<CatalogProductRecord> dataset) {
    //Save the RDD on disk. This RDD will be used later Delta
    String newRddPath = DataUtil.getNewPath();
    LOGGER.info("Writing RDD to file = " + newRddPath);
    dataset.write().parquet(newRddPath);
  }

  private static void subtract(Dataset<Row> first, Dataset<Row> second) {
    Dataset<Row> subtract = first.except(second);
    Dataset<Row> subtractBoolean = subtract.filter(new FilterFunction<Row>() {
      @Override
      public boolean call(Row row) throws Exception {
        LOGGER.error("Dropped record = " + row.getLong(0));
        return true;
      }
    });
    subtractBoolean.collectAsList();
  }

}
