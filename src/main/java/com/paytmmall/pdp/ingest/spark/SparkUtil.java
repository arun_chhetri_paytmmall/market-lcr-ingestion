package com.paytmmall.pdp.ingest.spark;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.when;

import com.paytmmall.pdp.config.Config;
import java.io.IOException;
import java.util.Arrays;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SparkUtil {

  private static Logger LOGGER = Logger.getLogger(SparkUtil.class);

  public static Dataset<Row> generateCategoryView(final SparkSession sparkSession)
      throws IOException {
    Column[] columnsToSelect = Arrays.stream(Constants.CATALOG_CATEGORY_COLUMNS).map(s -> col(s))
        .toArray(Column[]::new);
    Dataset<Row> categoryDataSet = DataSetGenerator
        .decorateCatalogCategorySparkSession(sparkSession).select(columnsToSelect);
    return filterAndGenerateCategoryView(categoryDataSet);
  }

  public static Dataset<Row> filterAndGenerateCategoryView(Dataset<Row> categoryDataset) {
    Dataset<Row> filterDataSet = categoryDataset
        .where(col(Constants.TYPE).equalTo(Integer.valueOf(1))) //Only select category with tree
        .where(col(Constants.STATUS).equalTo(Integer.valueOf(1))); //Only select category with status 1
    if (Config.getEnvironmentConfig().isDevEnvironment()) {
      //This is done to decrease the data set size on local development
      filterDataSet = filterDataSet.select("*")
          .where("id = 7164 or id = 7166 or id = 7182");
    }
    return buildCategoryJoin(filterDataSet);
  }

  public static Dataset<Row> generateCatalogProductView(final SparkSession sparkSession)
      throws IOException {
    Column[] columns = Arrays.stream(Constants.CATALOG_PRODUCT_COLUMNS).map(s -> col(s))
        .toArray(Column[]::new);
    Dataset<Row> productDataSet = DataSetGenerator.decorateCatalogProductSparkSession(sparkSession)
        .select(columns);
    return filterAndGenerateProductView(productDataSet);
  }

  //TODO: Join with merchant table and remove all products whose merchants are disabled
  //TODO: Join with catalog_brand table and remove all products whose brand status is not 1
  public static Dataset<Row> filterAndGenerateProductView(Dataset<Row> productDataSet) {
    Dataset<Row> filteredProductView = productDataSet
        .where(col(Constants.STATUS).equalTo(Integer.valueOf(1))) //Only use active products
        .where(col(Constants.CUSTOM_INT_5).equalTo(6)) //Only use live products
        .where(col(Constants.PRODUCT_TYPE).equalTo(1)); //Only simple;
    if (Config.getEnvironmentConfig().isDevEnvironment()) {
      //This is done to decrease the data set size on local development
      filteredProductView = filteredProductView.where("id = 18407352");
    }
    return filteredProductView;
  }

  public static Dataset<Row> generateInventoryView(final SparkSession sparkSession)
      throws IOException {
    Column[] columnsToSelect = Arrays.stream(Constants.INVENTORY_COLUMNS).map(s -> col(s))
        .toArray(Column[]::new);
    Dataset<Row> inventoryDataSet = DataSetGenerator.decorateInventorySparkSession(sparkSession)
        .select(columnsToSelect);
    if (Config.getEnvironmentConfig().isDevEnvironment()) {
      //This is done to decrease the data set size on local development
      inventoryDataSet = inventoryDataSet.where("product_id = 18407352");
    }
    return buildInventoryJoin(inventoryDataSet);
  }

  protected static Dataset<Row> buildInventoryJoin(Dataset<Row> inventoryDataSet) {
    return inventoryDataSet.groupBy(inventoryDataSet.col(Constants.PRODUCT_ID)).sum(Constants.QTY)
        .withColumnRenamed("sum(qty)", "inventory_qty");
  }

  /*
   * Always do left join on catalog_category and never left outer join.
   * This way, we will not pick any product whose category_id is not in catalog_category.
   * Also, with this logic, we will not pick any product whose category is disabled.
   * */
  public static Dataset<Row> joinCatalogProductAndInventoryAndCategory(
      final Dataset<Row> catalogProduct, final Dataset<Row> inventory,
      final Dataset<Row> category) {
    return catalogProduct.join(
        inventory, catalogProduct.col(Constants.ID).equalTo(inventory.col(Constants.PRODUCT_ID)),
        "leftouter"
    ).join(category, Constants.CATEGORY_ID);
  }

  private static String getFieldName(int level, String prefix, String suffix) {
    StringBuilder idFieldBuilder = new StringBuilder(prefix);
    idFieldBuilder.append("_");
    for (int i = 0; i < level; i++) {
      idFieldBuilder.append("parent_");
    }
    idFieldBuilder.append(suffix);
    return idFieldBuilder.toString();

  }

  private static String getIdFieldName(int level) {
    return getFieldName(level, "category", "id");
  }

  private static String getNameFieldName(int level) {
    return getFieldName(level, "category", "name");
  }

  private static String getParentIdFieldName(int level) {
    return getFieldName(level, "category_parent", "id");
  }

  private static String getLevelFieldName(int level) {
    return getFieldName(level, "category", "level");
  }

  /*
   * This function is used to create a flat map of catalog_category table.
   * This catalog_category_table will be joined with catalog_product table to create
   * flat representation of catalog_product
   * */
  protected static Dataset<Row> buildCategoryJoin(final Dataset<Row> categoryTableView) {
    LOGGER.debug("Building category join");
    Dataset<Row> first = categoryTableView
        .select(categoryTableView.col(Constants.ID).as(Constants.CATEGORY_ID),
            categoryTableView.col(Constants.NAME).as(Constants.CATEGORY_NAME),
            categoryTableView.col(Constants.PARENT_ID).as(Constants.CATEGORY_PARENT_ID),
            categoryTableView.col(Constants.LEVEL).as(Constants.CATEGORY_LEVEL));

    Dataset<Row> second = null;

    for (int i = 1; i < 5; i++) {
      LOGGER.debug("Category join level= " + Integer.toString(i));
      final String idFieldName = getIdFieldName(i - 1);
      final String nameFieldName = getNameFieldName(i - 1);
      final String parentIdFieldName = getParentIdFieldName(i - 1);
      final String levelFieldName = getLevelFieldName(i - 1);

      final String idParentFieldName = getIdFieldName(i);
      final String nameParentFieldName = getNameFieldName(i);
      final String parentParentIdFieldName = getParentIdFieldName(i);
      final String levelParentFieldName = getLevelFieldName(i);

      //This view is for parent of product level category
      //In this code, we are getting distinct parent from first result set.
      second = first
          .select(first.col(idFieldName).as(idParentFieldName),
              first.col(nameFieldName).as(nameParentFieldName),
              first.col(parentIdFieldName).as(parentParentIdFieldName),
              first.col(levelFieldName).as(levelParentFieldName)).distinct();

      //Now select certain columns from first and second dataset. This is done to keep one
      //category_id coming from first and second
      Column[] columns = new Column[first.columns().length + second.columns().length - 1];
      int columnIndex = 0;
      StringBuilder selectColumnForLogging = new StringBuilder();
      selectColumnForLogging.append("[");
      for (String firstColumn : first.columns()) {
        if (firstColumn.equals(parentIdFieldName)) {
          continue;
        }
        selectColumnForLogging.append(" (first) " + firstColumn + ",");
        columns[columnIndex] = first.col(firstColumn);
        columnIndex++;
      }

      for (String secondColumn : second.columns()) {
        selectColumnForLogging.append(" (second) " + secondColumn + ",");
        columns[columnIndex] = second.col(secondColumn);
        columnIndex++;
      }
      selectColumnForLogging.append("]");
      LOGGER.debug("Select condition for second = " + selectColumnForLogging.toString());

      Column joinCondition = when(
          first.col(levelFieldName).equalTo(0),
          first.col(idFieldName)
      ).otherwise(first.col(parentIdFieldName)).equalTo(second.col(idParentFieldName));

      LOGGER.debug("Join condition for first and second table = " + joinCondition.toString());
      //Use left join and not left outer here. This is done to drop all the categories which
      //Are disabled (status = 0) should not come in category view
      second = first.join(
          second,
          joinCondition
      ).select(columns);

      first = second;
    }
    return second;
  }
}
