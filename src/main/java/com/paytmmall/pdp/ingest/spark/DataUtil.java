package com.paytmmall.pdp.ingest.spark;

import com.paytmmall.pdp.config.Config;
import java.io.File;
import java.io.FilenameFilter;

public class DataUtil {

  public static String getOldDataPath() {
    String savedRddLocation = Config.getEnvironmentConfig().getSavedRddLocation();
    File userHome = new File(System.getProperty("user.home"));
    savedRddLocation = userHome.getPath() + "/" + savedRddLocation;
    //find the folder with latest timestamp under this directory
    File rootFile = new File(savedRddLocation);
    String[] directories = rootFile.list(new FilenameFilter() {
      @Override
      public boolean accept(File current, String name) {
        return new File(current, name).isDirectory();
      }
    });
    if (directories == null) {
      return null;
    }
    Long latestTimestamp = null;
    for (String directory : directories) {
      File file = new File(directory);
      if (latestTimestamp == null) {
        latestTimestamp = Long.valueOf(file.getName());
      } else if (Long.valueOf(file.getName()) > latestTimestamp) {
        latestTimestamp = Long.valueOf(file.getName());
      }
    }
    if (latestTimestamp == null) {
      return null;
    } else {
      return rootFile.getPath() + "/" + latestTimestamp.toString();
    }
  }

  public static String getNewPath() {
    return new File(new File(System.getProperty("user.home")) + "/" + Config.getEnvironmentConfig()
        .getSavedRddLocation()) + "/" + System
        .currentTimeMillis();
  }
}
