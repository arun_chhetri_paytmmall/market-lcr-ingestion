package com.paytmmall.pdp.ingest.spark;

public class Constants {

  public static final String TYPE = "TYPE".toLowerCase();
  public static String ID = "ID".toLowerCase();
  public static String NAME = "NAME".toLowerCase();
  public static String PARENT_ID = "parent_id";
  public static String LEVEL = "level";
  public static String MERCHANT_ID = "MERCHANT_ID".toLowerCase();
  public static String VALUE = "VALUE".toLowerCase();
  public static String PRODUCT_TYPE = "PRODUCT_TYPE".toLowerCase();
  public static String MRP = "MRP".toLowerCase();
  public static String PRICE = "PRICE".toLowerCase();
  public static String BRAND_ID = "BRAND_ID".toLowerCase();
  public static String BRAND = "BRAND".toLowerCase();
  public static String STATUS = "STATUS".toLowerCase();
  public static String VISIBILITY = "VISIBILITY".toLowerCase();
  public static String VERTICAL_ID = "VERTICAL_ID".toLowerCase();
  public static String ATTRIBUTES = "ATTRIBUTES".toLowerCase();
  public static String CREATED_AT = "CREATED_AT".toLowerCase();
  public static String UPDATED_AT = "UPDATED_AT".toLowerCase();
  public static String IS_IN_STOCK = "IS_IN_STOCK".toLowerCase();
  public static String PAY_TYPE = "PAY_TYPE".toLowerCase();
  public static String MERCHANT_NAME = "MERCHANT_NAME".toLowerCase();
  public static String CONV_FEE = "CONV_FEE".toLowerCase();
  public static String SHIPPING_CHARGE = "SHIPPING_CHARGE".toLowerCase();
  public static String BARGAIN = "BARGAIN".toLowerCase();
  public static String DISCOUNT = "DISCOUNT".toLowerCase();
  public static String MAX_DISPATCH_TIME = "MAX_DISPATCH_TIME".toLowerCase();
  public static String NEED_SHIPPING = "NEED_SHIPPING".toLowerCase();
  public static String FULFILLMENT_MODE = "FULFILLMENT_MODE".toLowerCase();
  public static String CUSTOM_INT_5 = "CUSTOM_INT_5".toLowerCase();

  //category specific code
  public static String CATEGORY_ID = "CATEGORY_ID".toLowerCase();
  public static String CATEGORY_NAME = "CATEGORY_NAME".toLowerCase();
  public static String CATEGORY_PARENT_ID = "CATEGORY_PARENT_ID".toLowerCase();
  public static String CATEGORY_LEVEL = "CATEGORY_LEVEL".toLowerCase();

  //specific for inventory
  public static String PRODUCT_ID = "PRODUCT_ID".toLowerCase();
  public static String QTY = "QTY".toLowerCase();
  public static String WAREHOUSE_ID = "WAREHOUSE_ID".toLowerCase();

  public static String[] CATALOG_PRODUCT_COLUMNS = {ID, NAME, PARENT_ID, MERCHANT_ID, VALUE,
      PRODUCT_TYPE, MRP, PRICE, BRAND_ID, BRAND, VISIBILITY, VERTICAL_ID, CATEGORY_ID,
      ATTRIBUTES, CREATED_AT, UPDATED_AT, IS_IN_STOCK, PAY_TYPE, MERCHANT_NAME, CONV_FEE,
      SHIPPING_CHARGE, BARGAIN, DISCOUNT, MAX_DISPATCH_TIME, NEED_SHIPPING, FULFILLMENT_MODE};


  public static String[] CATALOG_CATEGORY_COLUMNS = {ID, NAME, PARENT_ID, LEVEL};

  public static String[] INVENTORY_COLUMNS = {ID, PRODUCT_ID, QTY, WAREHOUSE_ID};
}
