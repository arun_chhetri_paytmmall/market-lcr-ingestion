package com.paytmmall.pdp.consumer.es;

import com.paytmmall.pdp.config.Config;
import com.paytmmall.pdp.consumer.BatchResponse;
import com.paytmmall.pdp.consumer.Consumer;
import com.paytmmall.pdp.consumer.ConsumerFactory;
import com.paytmmall.pdp.ingest.CatalogProductRecord;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import com.paytmmall.pdp.consumer.Util;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

public class ESConsumer implements Consumer {

  private static Logger LOGGER = Logger.getLogger(ESConsumer.class);
  private static RestHighLevelClient restHighLevelClient;

  public void consume(final CatalogProductRecord cpr) throws Exception {
    BatchResponse batchResponse = batchConsume(Arrays.asList(cpr).iterator());
    if (batchResponse.getAll().get(0).getSuccess()) {
      LOGGER.info("Inserted record = " + cpr.getId());
    } else {
      LOGGER.info(String.format("Failed insertion of record %s due to %s", cpr.getId().toString(),
          batchResponse.getAll().get(0).getError()));
    }
  }

  private BatchResponse batchConsume(final CatalogProductRecord[] catalogProductRecords)
      throws IOException {
    final BulkRequest bulkRequest = new BulkRequest();
    for (CatalogProductRecord catalogProductRecord : catalogProductRecords) {
      ImmutablePair<Long, String> cprPair = Util
          .convertCatalogProductRecordToJson(catalogProductRecord);
      IndexRequest indexRequest = new IndexRequest(
          Config.getEnvironmentConfig().getEsProductIndex(),
          Config.getEnvironmentConfig().getEsProductIndexType(),
          catalogProductRecord.getId().toString());
      indexRequest.source(cprPair.getRight(), XContentType.JSON);
      bulkRequest.add(indexRequest);
    }
    final BatchResponse batchResponse = new BatchResponse();
    BulkResponse bulkItemResponses = getRestClient().bulk(bulkRequest);
    for (BulkItemResponse bulkItemResponse : bulkItemResponses.getItems()) {
      final String id = bulkItemResponse.getId();
      final Boolean status = bulkItemResponse.getFailure() != null ? Boolean.FALSE : Boolean.TRUE;
      if (status) {
        batchResponse.addSuccess(id);
      } else {
        batchResponse.addFailure(id, bulkItemResponse.getFailure().getMessage());
      }
    }
    return batchResponse;
  }

  public BatchResponse batchConsume(final Iterator<CatalogProductRecord> cprIterator)
      throws Exception {
    final BatchResponse batchResponse = new BatchResponse();
    List<CatalogProductRecord> recordList = new ArrayList<>();
    while (cprIterator.hasNext()) {
      recordList.add(cprIterator.next());
      if (recordList.size() >= 100) {

        batchResponse.addAll(
            batchConsume(recordList.toArray(new CatalogProductRecord[recordList.size()])).getAll()
        );
        recordList = new ArrayList<>();
      }
    }
    if (recordList.size() > 0) {
      batchResponse.addAll(
          batchConsume(recordList.toArray(new CatalogProductRecord[recordList.size()])).getAll()
      );
    }
    return batchResponse;
  }

  @Override
  public void delete(final CatalogProductRecord catalogProductRecord) throws IOException {
    DeleteResponse deleteResponse = getRestClient().delete(
        new DeleteRequest("product", "product", catalogProductRecord.getId().toString())
    );
    LOGGER.info("Deleted record = " + catalogProductRecord.getId());
  }

  private static RestHighLevelClient getRestClient() {
    if (restHighLevelClient == null) {
      synchronized (ESConsumer.class) {
        if (restHighLevelClient == null) {
          List<HttpHost> httpHosts = Config.getEnvironmentConfig().getEsHosts().stream()
              .map(esHost -> HttpHost.create(esHost)).collect(Collectors.<HttpHost>toList());
          restHighLevelClient = new RestHighLevelClient(
              RestClient.builder(httpHosts
                  .toArray(new HttpHost[Config.getEnvironmentConfig().getEsHosts().size()]))
          );
        }
      }
    }
    return restHighLevelClient;
  }

  @Override
  public void close() {
    try {
      restHighLevelClient.close();
    } catch (Exception ex) {
      LOGGER.error("Exception while closing elastic search. Nothing to do here.", ex);
    }
  }
}
