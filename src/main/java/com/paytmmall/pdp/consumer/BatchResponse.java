package com.paytmmall.pdp.consumer;

import java.util.ArrayList;
import java.util.List;

public class BatchResponse {
  private List<PResponse> PResponseList;

  public BatchResponse() {
    this.PResponseList = new ArrayList<>();
  }

  public void addSuccess(final String id) {
    this.PResponseList.add(new PResponse(id, Boolean.TRUE, null));
  }

  public void addFailure(final String id, final String error) {
    this.PResponseList.add(new PResponse(id, Boolean.FALSE, error));
  }

  public void addAll(final List<PResponse> responseList) {
    this.PResponseList.addAll(responseList);
  }

  public void addResponse(final PResponse PResponse) {
    this.PResponseList.add(PResponse);
  }

  public List<PResponse> getAll() {
    return this.PResponseList;
  }

  public static class PResponse {

    private final String id;
    private final Boolean success;
    private final String error;

    public PResponse(final String id, final Boolean success, final String error){
      this.id = id;
      this.success = success;
      this.error = error;
    }

    public String getId() {
      return id;
    }

    public Boolean getSuccess() {
      return success;
    }

    public String getError() {
      return error;
    }
  }
}
