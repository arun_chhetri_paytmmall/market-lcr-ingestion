package com.paytmmall.pdp.consumer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytmmall.pdp.ingest.CatalogProductRecord;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;

public class Util {

  private static Logger LOGGER = Logger.getLogger(Util.class);
  private static ObjectMapper objectMapper = new ObjectMapper();

  public static StorageRecord convertCatalogProductRecordToStorageRecord(
      final CatalogProductRecord catalogProductRecord) {
    final StorageRecord storageRecord = new StorageRecord();
    storageRecord.setId(catalogProductRecord.getId());
    storageRecord.setParent_id(catalogProductRecord.getParent_id());
    storageRecord.setMerchant_id(catalogProductRecord.getMerchant_id());
    storageRecord.setMerchant_name(catalogProductRecord.getMerchant_name());
    storageRecord.setProduct_type(catalogProductRecord.getProduct_type());
    storageRecord.setName(catalogProductRecord.getName());
    storageRecord.setValue(catalogProductRecord.getValue());
    storageRecord.setMrp(catalogProductRecord.getMrp());
    storageRecord.setPrice(catalogProductRecord.getPrice());
    storageRecord.setBrand_id(catalogProductRecord.getBrand_id());
    storageRecord.setBrand(catalogProductRecord.getBrand());
    storageRecord.setVisibility(catalogProductRecord.getVisibility());
    storageRecord.setVertical_id(catalogProductRecord.getVertical_id());
    storageRecord.setCreated_at(catalogProductRecord.getCreated_at());
    storageRecord.setUpdated_at(catalogProductRecord.getUpdated_at());
    storageRecord.setIs_in_stock(catalogProductRecord.getIs_in_stock());
    storageRecord.setPay_type(catalogProductRecord.getPay_type());
    storageRecord.setConv_fee(catalogProductRecord.getConv_fee());
    storageRecord.setShipping_charge(catalogProductRecord.getShipping_charge());
    storageRecord.setBargain(catalogProductRecord.getBargain());
    storageRecord.setDiscount(catalogProductRecord.getDiscount());
    storageRecord.setMax_dispatch_time(catalogProductRecord.getMax_dispatch_time());
    storageRecord.setNeed_shipping(catalogProductRecord.getNeed_shipping());
    storageRecord.setFulfillment_mode(catalogProductRecord.getFulfillment_mode());

    //category_related_fields
    //current record category fields
    storageRecord.setCategory_id(catalogProductRecord.getCategory_id());
    storageRecord.setCategory_level(catalogProductRecord.getCategory_level());
    storageRecord.setCategory_name(catalogProductRecord.getCategory_name());

    //Now looking at the level put the fields
    Long[] categories = new Long[catalogProductRecord.getCategory_level() + 1];
    String[] categories_name = new String[catalogProductRecord.getCategory_level() + 1];

    if (catalogProductRecord.getCategory_level() == 0) {
      categories[0] = catalogProductRecord.getCategory_id();
      categories_name[0] = catalogProductRecord.getCategory_name();
    } else if (catalogProductRecord.getCategory_level() == 1) {
      categories[0] = catalogProductRecord.getCategory_parent_id();
      categories_name[0] = catalogProductRecord.getCategory_parent_name();
      categories[1] = catalogProductRecord.getCategory_id();
      categories_name[1] = catalogProductRecord.getCategory_name();
    } else if (catalogProductRecord.getCategory_level() == 2) {
      categories[0] = catalogProductRecord.getCategory_parent_parent_id();
      categories_name[0] = catalogProductRecord.getCategory_parent_parent_name();
      categories[1] = catalogProductRecord.getCategory_parent_id();
      categories_name[1] = catalogProductRecord.getCategory_parent_name();
      categories[2] = catalogProductRecord.getCategory_id();
      categories_name[2] = catalogProductRecord.getCategory_name();
    } else if (catalogProductRecord.getCategory_level() == 3) {
      categories[0] = catalogProductRecord.getCategory_parent_parent_parent_id();
      categories_name[0] = catalogProductRecord.getCategory_parent_parent_parent_name();
      categories[1] = catalogProductRecord.getCategory_parent_parent_id();
      categories_name[1] = catalogProductRecord.getCategory_parent_parent_name();
      categories[2] = catalogProductRecord.getCategory_parent_id();
      categories_name[2] = catalogProductRecord.getCategory_parent_name();
      categories[3] = catalogProductRecord.getCategory_id();
      categories_name[3] = catalogProductRecord.getCategory_name();
    } else {
      categories[0] = catalogProductRecord.getCategory_parent_parent_parent_parent_id();
      categories_name[0] = catalogProductRecord.getCategory_parent_parent_parent_parent_name();
      categories[1] = catalogProductRecord.getCategory_parent_parent_parent_id();
      categories_name[1] = catalogProductRecord.getCategory_parent_parent_parent_name();
      categories[2] = catalogProductRecord.getCategory_parent_parent_id();
      categories_name[2] = catalogProductRecord.getCategory_parent_parent_name();
      categories[3] = catalogProductRecord.getCategory_parent_id();
      categories_name[3] = catalogProductRecord.getCategory_parent_name();
      categories[4] = catalogProductRecord.getCategory_id();
      categories_name[4] = catalogProductRecord.getCategory_name();
    }
    storageRecord.setCategory_level_array(categories);
    storageRecord.setCategory_name_array(categories_name);

    //Inventory fields
    storageRecord.setQty(catalogProductRecord.getInventory_qty());
    return storageRecord;
  }

  //TODO: Handle json in attribute values as well
  //"bottom_length":{"val":"2","unit":"Mtr"}
  // "dupatta_length":{"val":"2.25","unit":"Mtr"},"style_code":"2001",
  // "top_length":{"val":"2.5","unit":"Mtr"}}'
  public static ImmutablePair<Long, String> convertCatalogProductRecordToJson(
      final CatalogProductRecord catalogProductRecord) throws IOException {
    LOGGER.debug("Input catalog record = " + catalogProductRecord.toString());
    String attributes = catalogProductRecord.getAttributes();
    final StorageRecord sr = convertCatalogProductRecordToStorageRecord(catalogProductRecord);
    String jsonValue = objectMapper.writeValueAsString(sr);
    if (attributes != null && attributes.length() > 0) {
      Map<String, Object> attributeMap = getAttributeMap(attributes);
      if (attributeMap != null) {
        Map<String, Object> srMap = objectMapper.readValue(jsonValue, Map.class);
        srMap.putAll(attributeMap);
        jsonValue = objectMapper.writeValueAsString(srMap);
      }
    }
    LOGGER.debug("Output storage record = " + jsonValue);
    return ImmutablePair.of(catalogProductRecord.getId(), jsonValue);
  }

  protected static Map<String, Object> getAttributeMap(final String attributes) throws IOException {
    Map<String, Object> attributeMap = null;
    try {
      attributeMap = objectMapper.readValue(attributes, Map.class);
    } catch (JsonParseException jpe) {
      LOGGER.info(String.format("Invalid attributes field. Removing it. = %s", attributes), jpe);
      try {
        attributeMap = objectMapper.readValue(removeInvalidJsonCharacters(attributes), Map.class);
      } catch (JsonParseException jpe2) {
        LOGGER.error(String
            .format("Not able to remove invalid attributes. Dropping attributes field = %s",
                attributes), jpe2);
      }
    }
    Map<String, Object> resultMap = null;

    if (attributeMap != null && !attributeMap.isEmpty()) {
      resultMap = new HashMap<>();
      for (Map.Entry<String, Object> attributeEntry : attributeMap.entrySet()) {
        if (attributeEntry.getValue() instanceof Map) {
          Map<String, Object> valueMap = (Map<String, Object>) attributeEntry.getValue();
          StringBuilder innerValueBuilder = new StringBuilder();
          for (Map.Entry<String, Object> innerEntry : valueMap.entrySet()) {
            innerValueBuilder.append(innerEntry.getValue());
            innerValueBuilder.append(" ");
          }
          String innerValue = innerValueBuilder.toString().trim();
          resultMap.put("attr_" + attributeEntry.getKey(), innerValue);
        } else {
          resultMap.put("attr_" + attributeEntry.getKey(), attributeEntry.getValue());
        }
      }
    }
    return resultMap;
  }

  private static String removeInvalidJsonCharacters(final String jsonString) {
    if (jsonString == null) {
      return jsonString;
    }
    final StringBuilder jsonStringBuilder = new StringBuilder();
    for (int i = 0; i < jsonString.length(); i++) {
      char ch = jsonString.charAt(i);
      switch (ch) {
        case '\b':
          break;
        case '\f':
          break;
        case '\n':
          break;
        case '\r':
          break;
        case '\t':
          break;
        case '\\':
          break;
        default:
          jsonStringBuilder.append(ch);
          break;
      }
    }
    return jsonStringBuilder.toString();
  }
}