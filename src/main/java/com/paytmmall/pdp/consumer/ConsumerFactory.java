package com.paytmmall.pdp.consumer;

import com.paytmmall.pdp.consumer.es.ESConsumer;

public class ConsumerFactory {

  private static ESConsumer esConsumer;

  public static Consumer getConsumer() {
    if (esConsumer == null) {
      synchronized (ConsumerFactory.class) {
        if (esConsumer == null) {
          esConsumer = new ESConsumer();
        }
      }
    }
    return esConsumer;
  }
}
