package com.paytmmall.pdp.consumer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;

@JsonInclude(Include.NON_NULL)
public class StorageRecord implements Serializable {

  private Long id; //Id of the product
  private Long parent_id; //parent ID of the product
  private Long merchant_id;
  private String merchant_name;
  private Integer product_type;
  private String name;
  private Integer value;
  private BigDecimal mrp;
  private BigDecimal price;
  private Long brand_id;
  private String brand;
  private Integer visibility;
  private Long vertical_id;

  private Timestamp created_at;
  private Timestamp updated_at;
  private Integer is_in_stock;
  private Integer pay_type;
  private Integer conv_fee;
  private BigDecimal shipping_charge;
  private Integer bargain;
  private Integer discount;
  private Integer max_dispatch_time;
  private Integer need_shipping;
  private Integer fulfillment_mode;

  private Long category_id;
  private Integer category_level;
  private String category_name;

  private Long[] category_level_array;
  private String[] category_name_array;

  private Long inventory_qty;

  public StorageRecord() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public Long getMerchant_id() {
    return merchant_id;
  }

  public void setMerchant_id(Long merchant_id) {
    this.merchant_id = merchant_id;
  }

  public String getMerchant_name() {
    return merchant_name;
  }

  public void setMerchant_name(String merchant_name) {
    this.merchant_name = merchant_name;
  }

  public Integer getProduct_type() {
    return product_type;
  }

  public void setProduct_type(Integer product_type) {
    this.product_type = product_type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public BigDecimal getMrp() {
    return mrp;
  }

  public void setMrp(BigDecimal mrp) {
    this.mrp = mrp;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Long getBrand_id() {
    return brand_id;
  }

  public void setBrand_id(Long brand_id) {
    this.brand_id = brand_id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public Integer getVisibility() {
    return visibility;
  }

  public void setVisibility(Integer visibility) {
    this.visibility = visibility;
  }

  public Long getVertical_id() {
    return vertical_id;
  }

  public void setVertical_id(Long vertical_id) {
    this.vertical_id = vertical_id;
  }

  public Timestamp getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Timestamp created_at) {
    this.created_at = created_at;
  }

  public Timestamp getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Timestamp updated_at) {
    this.updated_at = updated_at;
  }

  public Integer getIs_in_stock() {
    return is_in_stock;
  }

  public void setIs_in_stock(Integer is_in_stock) {
    this.is_in_stock = is_in_stock;
  }

  public Integer getPay_type() {
    return pay_type;
  }

  public void setPay_type(Integer pay_type) {
    this.pay_type = pay_type;
  }

  public Integer getConv_fee() {
    return conv_fee;
  }

  public void setConv_fee(Integer conv_fee) {
    this.conv_fee = conv_fee;
  }

  public BigDecimal getShipping_charge() {
    return shipping_charge;
  }

  public void setShipping_charge(BigDecimal shipping_charge) {
    this.shipping_charge = shipping_charge;
  }

  public Integer getBargain() {
    return bargain;
  }

  public void setBargain(Integer bargain) {
    this.bargain = bargain;
  }

  public Integer getDiscount() {
    return discount;
  }

  public void setDiscount(Integer discount) {
    this.discount = discount;
  }

  public Integer getMax_dispatch_time() {
    return max_dispatch_time;
  }

  public void setMax_dispatch_time(Integer max_dispatch_time) {
    this.max_dispatch_time = max_dispatch_time;
  }

  public Integer getNeed_shipping() {
    return need_shipping;
  }

  public void setNeed_shipping(Integer need_shipping) {
    this.need_shipping = need_shipping;
  }

  public Integer getFulfillment_mode() {
    return fulfillment_mode;
  }

  public void setFulfillment_mode(Integer fulfillment_mode) {
    this.fulfillment_mode = fulfillment_mode;
  }

  public Long getCategory_id() {
    return category_id;
  }

  public void setCategory_id(Long category_id) {
    this.category_id = category_id;
  }

  public Integer getCategory_level() {
    return category_level;
  }

  public void setCategory_level(Integer category_level) {
    this.category_level = category_level;
  }

  public String getCategory_name() {
    return category_name;
  }

  public void setCategory_name(String category_name) {
    this.category_name = category_name;
  }

  public Long[] getCategory_level_array() {
    return category_level_array;
  }

  public void setCategory_level_array(Long[] category_level_array) {
    this.category_level_array = category_level_array;
  }

  public String[] getCategory_name_array() {
    return category_name_array;
  }

  public void setCategory_name_array(String[] category_name_array) {
    this.category_name_array = category_name_array;
  }

  public Long getInventory_qty() {
    return inventory_qty;
  }

  public void setQty(Long inventory_qty) {
    this.inventory_qty = inventory_qty;
  }

  @Override
  public String toString() {
    return "StorageRecord{" +
        "id=" + id +
        ", parent_id=" + parent_id +
        ", merchant_id=" + merchant_id +
        ", merchant_name='" + merchant_name + '\'' +
        ", product_type=" + product_type +
        ", name='" + name + '\'' +
        ", value=" + value +
        ", mrp=" + mrp +
        ", price=" + price +
        ", brand_id=" + brand_id +
        ", brand='" + brand + '\'' +
        ", visibility=" + visibility +
        ", vertical_id=" + vertical_id +
        ", created_at=" + created_at +
        ", updated_at=" + updated_at +
        ", is_in_stock=" + is_in_stock +
        ", pay_type=" + pay_type +
        ", conv_fee=" + conv_fee +
        ", shipping_charge=" + shipping_charge +
        ", bargain=" + bargain +
        ", discount=" + discount +
        ", max_dispatch_time=" + max_dispatch_time +
        ", need_shipping=" + need_shipping +
        ", fulfillment_mode=" + fulfillment_mode +
        ", category_id=" + category_id +
        ", category_level=" + category_level +
        ", category_name='" + category_name + '\'' +
        ", category_level_array=" + Arrays.toString(category_level_array) +
        ", category_name_array=" + Arrays.toString(category_name_array) +
        ", inventory_qty=" + inventory_qty +
        '}';
  }
}