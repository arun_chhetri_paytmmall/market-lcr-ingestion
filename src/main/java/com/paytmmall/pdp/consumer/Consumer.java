package com.paytmmall.pdp.consumer;

import com.paytmmall.pdp.ingest.CatalogProductRecord;
import java.io.IOException;
import java.util.Iterator;

public interface Consumer {

  void consume(final CatalogProductRecord cpr) throws Exception;

  void delete(final CatalogProductRecord catalogProductRecord) throws IOException;

  BatchResponse batchConsume(final Iterator<CatalogProductRecord> catalogProductRecordList) throws Exception;

  void close();
}
