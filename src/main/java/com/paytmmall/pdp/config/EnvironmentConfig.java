package com.paytmmall.pdp.config;

import java.io.Serializable;
import java.util.List;

public class EnvironmentConfig implements Serializable {

  private String applicationName;
  private String sparkMasterURL;
  private String env;

  private String catalogProductSourceType;
  private String catalogProductSourceTypeURL;
  private String catalogProductjdbcUser;
  private String catalogProductjdbcPassword;

  private String catalogCategorySourceType;
  private String catalogCategorySourceTypeURL;
  private String catalogCategoryjdbcUser;
  private String catalogCategoryjdbcPassword;

  private String inventorySourceType;
  private String inventorySourceTypeURL;
  private String inventoryjdbcUser;
  private String inventoryjdbcPassword;

  private List<String> esHosts;
  private String esProductIndex;
  private String esProductIndexType;

  private String datasetApiEndpoint;
  private String savedRddLocation;

  public EnvironmentConfig() {
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public String getSparkMasterURL() {
    return sparkMasterURL;
  }

  public void setSparkMasterURL(String sparkMasterURL) {
    this.sparkMasterURL = sparkMasterURL;
  }

  public String getEnv() {
    return env;
  }

  public void setEnv(String env) {
    this.env = env;
  }

  public String getCatalogProductSourceType() {
    return catalogProductSourceType;
  }

  public void setCatalogProductSourceType(String catalogProductSourceType) {
    this.catalogProductSourceType = catalogProductSourceType;
  }

  public String getCatalogProductSourceTypeURL() {
    return catalogProductSourceTypeURL;
  }

  public void setCatalogProductSourceTypeURL(String catalogProductSourceTypeURL) {
    this.catalogProductSourceTypeURL = catalogProductSourceTypeURL;
  }

  public String getCatalogProductjdbcUser() {
    return catalogProductjdbcUser;
  }

  public void setCatalogProductjdbcUser(String catalogProductjdbcUser) {
    this.catalogProductjdbcUser = catalogProductjdbcUser;
  }

  public String getCatalogProductjdbcPassword() {
    return catalogProductjdbcPassword;
  }

  public void setCatalogProductjdbcPassword(String catalogProductjdbcPassword) {
    this.catalogProductjdbcPassword = catalogProductjdbcPassword;
  }

  public String getCatalogCategorySourceType() {
    return catalogCategorySourceType;
  }

  public void setCatalogCategorySourceType(String catalogCategorySourceType) {
    this.catalogCategorySourceType = catalogCategorySourceType;
  }

  public String getCatalogCategorySourceTypeURL() {
    return catalogCategorySourceTypeURL;
  }

  public void setCatalogCategorySourceTypeURL(String catalogCategorySourceTypeURL) {
    this.catalogCategorySourceTypeURL = catalogCategorySourceTypeURL;
  }

  public String getCatalogCategoryjdbcUser() {
    return catalogCategoryjdbcUser;
  }

  public void setCatalogCategoryjdbcUser(String catalogCategoryjdbcUser) {
    this.catalogCategoryjdbcUser = catalogCategoryjdbcUser;
  }

  public String getCatalogCategoryjdbcPassword() {
    return catalogCategoryjdbcPassword;
  }

  public void setCatalogCategoryjdbcPassword(String catalogCategoryjdbcPassword) {
    this.catalogCategoryjdbcPassword = catalogCategoryjdbcPassword;
  }

  public String getInventorySourceType() {
    return inventorySourceType;
  }

  public void setInventorySourceType(String inventorySourceType) {
    this.inventorySourceType = inventorySourceType;
  }

  public String getInventorySourceTypeURL() {
    return inventorySourceTypeURL;
  }

  public void setInventorySourceTypeURL(String inventorySourceTypeURL) {
    this.inventorySourceTypeURL = inventorySourceTypeURL;
  }

  public String getInventoryjdbcUser() {
    return inventoryjdbcUser;
  }

  public void setInventoryjdbcUser(String inventoryjdbcUser) {
    this.inventoryjdbcUser = inventoryjdbcUser;
  }

  public String getInventoryjdbcPassword() {
    return inventoryjdbcPassword;
  }

  public void setInventoryjdbcPassword(String inventoryjdbcPassword) {
    this.inventoryjdbcPassword = inventoryjdbcPassword;
  }

  public List<String> getEsHosts() {
    return esHosts;
  }

  public void setEsHosts(List<String> esHosts) {
    this.esHosts = esHosts;
  }

  public boolean isDevEnvironment() {
    return env.equals("dev");
  }

  public boolean isAwsEnvironment() {
    return env.equals("aws");
  }

  public String getDatasetApiEndpoint() {
    return datasetApiEndpoint;
  }

  public void setDatasetApiEndpoint(String datasetApiEndpoint) {
    this.datasetApiEndpoint = datasetApiEndpoint;
  }

  public String getSavedRddLocation() {
    return savedRddLocation;
  }

  public void setSavedRddLocation(String savedRddLocation) {
    this.savedRddLocation = savedRddLocation;
  }

  public String getEsProductIndex() {
    return esProductIndex;
  }

  public void setEsProductIndex(String esProductIndex) {
    this.esProductIndex = esProductIndex;
  }

  public String getEsProductIndexType() {
    return esProductIndexType;
  }

  public void setEsProductIndexType(String esProductIndexType) {
    this.esProductIndexType = esProductIndexType;
  }
}
