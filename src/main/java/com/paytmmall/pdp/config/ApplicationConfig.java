package com.paytmmall.pdp.config;

import java.io.Serializable;
import java.util.Map;

public class ApplicationConfig implements Serializable {

  private Map<String, EnvironmentConfig> environments;

  public ApplicationConfig() {
  }

  public Map<String, EnvironmentConfig> getEnvironments() {
    return environments;
  }

  public void setEnvironments(Map<String, EnvironmentConfig> environments) {
    this.environments = environments;
  }
}
