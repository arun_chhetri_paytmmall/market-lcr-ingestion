package com.paytmmall.pdp.config;

public class Config {
  private static EnvironmentConfig environmentConfig;
  private static ApplicationConfig applicationConfig;

  public static EnvironmentConfig getEnvironmentConfig() {
    return environmentConfig;
  }

  public static ApplicationConfig getApplicationConfig() {
    return applicationConfig;
  }

  public static void setConfig(ApplicationConfig appConfig,
      EnvironmentConfig envConfig) {
    environmentConfig = envConfig;
    applicationConfig = appConfig;
  }
}
