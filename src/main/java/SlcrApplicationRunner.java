import com.paytmmall.pdp.config.ApplicationConfig;
import com.paytmmall.pdp.config.Config;
import com.paytmmall.pdp.config.EnvironmentConfig;
import com.paytmmall.pdp.ingest.spark.SparkIngestor;
import java.io.InputStream;
import org.apache.log4j.PropertyConfigurator;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class SlcrApplicationRunner {

  public static void main(String args[]) throws Exception {
    if (args.length < 1) {
      System.out.println("Not enough arguments. Exiting");
      return;
    }
    String environment = args[0];
    if (!loadConfig(environment)) {
      System.out.println("Env not provided or incorrect");
      return;
    }
    //configure logging file
    PropertyConfigurator
        .configure(SlcrApplicationRunner.class.getResourceAsStream("log4j.properties"));
    SparkIngestor.ingest();
  }

  private static boolean loadConfig(final String env) {
    Constructor constructor = new Constructor(ApplicationConfig.class);
    Yaml yaml = new Yaml(constructor);
    InputStream inputStream = SlcrApplicationRunner.class.getResourceAsStream("application.yml");
    ApplicationConfig applicationConfig = yaml.loadAs(inputStream, ApplicationConfig.class);
    final EnvironmentConfig environmentConfig = applicationConfig.getEnvironments().get(env);
    if (environmentConfig == null) {
      return false;
    }
    Config.setConfig(applicationConfig, environmentConfig);
    return true;
  }
}
