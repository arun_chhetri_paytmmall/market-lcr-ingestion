package com.paytmmall.pdp.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytmmall.pdp.ingest.CatalogProductRecord;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class UtilTest {

  private static ObjectMapper objectMapper;

  @BeforeClass
  public static void init() {
    objectMapper = new ObjectMapper();
  }

  @Test
  public void testConvertCatalogProductRecordToStorageRecord() throws Exception {
    CatalogProductRecord cpr = generateCatalogProductRecord();
    ImmutablePair<Long, String> idJsonPair = Util.convertCatalogProductRecordToJson(cpr);

    //Assert both the Id's are same
    Assert.assertEquals(cpr.getId(), idJsonPair.getLeft());
    Map<String, Object> values = objectMapper.readValue(idJsonPair.getRight(), Map.class);

    for (Method method : CatalogProductRecord.class.getDeclaredMethods()) {
      if (method.getName().startsWith("get") && !(
          method.getName().toLowerCase().endsWith("attributes") || method.getName().toLowerCase()
              .contains("category_parent"))) {
        Object result = method.invoke(cpr);
        if (result instanceof Timestamp) {
          result = ((Timestamp) result).getTime();
        }
        String fieldName = method.getName().replaceFirst("get", "").toLowerCase();
        Object result2 = values.get(fieldName);
        if (result == null && result2 == null) {
          continue; //both should be null
        } else if ((result == null && result2 != null) || (result != null && result2 == null)) {
          throw new RuntimeException("This is wrong");
        }
        if (result.getClass().equals(Long.class) && result2.getClass().equals(Integer.class)) {
          result2 = new Long((Integer) result2);
        }
        if (result instanceof BigDecimal && result2 instanceof Integer) {
          Assert.assertEquals(((BigDecimal) result).intValue(), ((Integer) result2).intValue());
        } else {
          Assert.assertEquals(result, result2);
        }
      }
    }

    if (cpr.getAttributes() != null) {
      Map<String, Object> cprAttributes = objectMapper.readValue(cpr.getAttributes(), Map.class);
      for (final Map.Entry<String, Object> entry : cprAttributes.entrySet()) {
        Assert.assertEquals(values.get("attr_" + entry.getKey()), entry.getValue());
      }
    }
    if (cpr.getCategory_id() > 0) {
      ArrayList categoryLevelArray = (ArrayList) values.get("category_level_array");
      ArrayList categoryNameArray = (ArrayList) values.get("category_name_array");

      ArrayList<Integer> expectedLevelArrayList = new ArrayList<>();
      ArrayList<String> expectedNameArrayList = new ArrayList<>();
      for (int i = 0; i <= cpr.getCategory_id().intValue(); i++) {
        expectedLevelArrayList.add(i);
        if (i == 0) {
          expectedNameArrayList.add("zero");
        } else if (i == 1) {
          expectedNameArrayList.add("one");
        } else if (i == 2) {
          expectedNameArrayList.add("two");
        } else if (i == 3) {
          expectedNameArrayList.add("three");
        } else if (i == 4) {
          expectedNameArrayList.add("four");
        } else {
          throw new RuntimeException("Not an expected level");
        }
      }
      Assert.assertEquals(expectedLevelArrayList, categoryLevelArray);
      Assert.assertEquals(expectedNameArrayList, categoryNameArray);
    }
  }

  @Test
  public void testAttributesMap() throws Exception {
    String testAttributes = "{\n"
        + "\t\"name\": \"test\",\n"
        + "\t\"color\": \"red\",\n"
        + "\t\"size\": {\n"
        + "\t\t\"val\": 20,\n"
        + "\t\t\"unit\": \"mtr\"\n"
        + "\t}\n"
        + "}";
    Map<String, Object> attributeMap = Util.getAttributeMap(testAttributes);
    Assert.assertEquals("test", attributeMap.get("attr_name"));
    Assert.assertEquals("red", attributeMap.get("attr_color"));
    Assert.assertEquals("20 mtr", attributeMap.get("attr_size"));
  }

  private CatalogProductRecord generateCatalogProductRecord() throws Exception {
    Random random = new Random();
    CatalogProductRecord cpr = new CatalogProductRecord();
    cpr.setId(random.nextLong());
    cpr.setName(Long.toString(cpr.getId()));
    cpr.setParent_id(random.nextLong());
    cpr.setMerchant_id(random.nextLong());
    cpr.setMerchant_name(Long.toString(cpr.getMerchant_id()));
    cpr.setValue(random.nextInt());
    cpr.setProduct_type(random.nextInt());
    cpr.setMrp(BigDecimal.valueOf(random.nextInt()));
    cpr.setPrice(BigDecimal.valueOf(random.nextInt()));
    cpr.setBrand_id(random.nextLong());
    cpr.setBrand(Long.toString(cpr.getBrand_id()));
    cpr.setVisibility(random.nextInt());
    cpr.setVertical_id(random.nextLong());
    cpr.setCreated_at(Timestamp.from(Instant.now()));
    cpr.setUpdated_at(Timestamp.from(Instant.now()));
    cpr.setIs_in_stock(random.nextInt());
    cpr.setPay_type(random.nextInt());
    cpr.setConv_fee(random.nextInt());
    cpr.setShipping_charge(BigDecimal.valueOf(random.nextInt()));
    cpr.setBargain(random.nextInt());
    cpr.setDiscount(random.nextInt());
    cpr.setMax_dispatch_time(random.nextInt());
    cpr.setNeed_shipping(random.nextInt());
    cpr.setFulfillment_mode(random.nextInt());
    cpr.setInventory_qty(random.nextLong());
    cpr.setCategory_id(4l);
    cpr.setCategory_level(4);
    cpr.setCategory_name("four");

    cpr.setCategory_parent_id(3l);
    cpr.setCategory_parent_name("three");
    cpr.setCategory_parent_level(3);

    cpr.setCategory_parent_parent_id(2l);
    cpr.setCategory_parent_parent_name("two");
    cpr.setCategory_parent_parent_level(2);

    cpr.setCategory_parent_parent_parent_id(1l);
    cpr.setCategory_parent_parent_parent_name("one");
    cpr.setCategory_parent_parent_parent_level(1);

    cpr.setCategory_parent_parent_parent_parent_id(0l);
    cpr.setCategory_parent_parent_parent_parent_name("zero");
    cpr.setCategory_parent_parent_parent_parent_level(0);

    Map<String, String> attributesMap = new HashMap<>();
    attributesMap.put("test", Integer.toString(random.nextInt()));
    attributesMap.put("colour", Long.toString(random.nextLong()));
    cpr.setAttributes(objectMapper.writeValueAsString(attributesMap));
    return cpr;
  }
}
