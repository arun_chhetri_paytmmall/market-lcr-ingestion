package com.paytmmall.pdp.ingest.spark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SparkData {

  public static List<CatalogCategory> generateLevelFiveCategory() {
    CatalogCategory zero = new CatalogCategory(0l, "zero", null, 0);
    CatalogCategory one = new CatalogCategory(1l, "one", 0l, 1);
    CatalogCategory two = new CatalogCategory(2l, "two", 1l, 2);
    CatalogCategory three = new CatalogCategory(3l, "three", 2l, 3);
    CatalogCategory four = new CatalogCategory(4l, "four", 3l, 4);
    return Arrays.asList(zero, one, two, three, four);
  }

  public static List<InventoryRecord> generateInventoryRecordForProduct(long count,
      long product_id) {
    InventoryRecord first = new InventoryRecord(1l, product_id, 1l, count / 2);
    InventoryRecord second = new InventoryRecord(1l, product_id, 1l, count - count / 2);
    return Arrays.asList(first, second);
  }

  public static List<CatalogProduct> generateCatalogProductRecords(int count,
      List<Long> product_ids,
      List<Long> category_ids) {
    List<CatalogProduct> catalogProducts = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      catalogProducts.add(generateCatalogProductRecord(product_ids.get(i), category_ids.get(i)));
    }
    return catalogProducts;
  }

  public static CatalogProduct generateCatalogProductRecord(Long product_id,
      Long category_id) {
    CatalogProduct cpr =  new CatalogProduct();
    cpr.id = product_id;
    cpr.category_id = category_id;
    cpr.merchant_id = product_id;
    cpr.parent_id = product_id;
    cpr.status = 1;
    cpr.custom_int_5 = 6;
    cpr.product_type = 1;
    return cpr;
  }

  public static class InventoryRecord {

    private Long id;
    private Long product_id;
    private Long warehouse_id;
    private Long qty;

    public InventoryRecord(Long id, Long product_id, Long warehouse_id, Long qty) {
      this.id = id;
      this.product_id = product_id;
      this.warehouse_id = warehouse_id;
      this.qty = qty;
    }

    public InventoryRecord() {
    }

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public Long getProduct_id() {
      return product_id;
    }

    public void setProduct_id(Long product_id) {
      this.product_id = product_id;
    }

    public Long getWarehouse_id() {
      return warehouse_id;
    }

    public void setWarehouse_id(Long warehouse_id) {
      this.warehouse_id = warehouse_id;
    }

    public Long getQty() {
      return qty;
    }

    public void setQty(Long qty) {
      this.qty = qty;
    }
  }

  public static class CatalogCategory {

    private Long id;
    private String name;
    private Long parent_id;
    private Integer level;
    private Integer status;
    private Integer type;
    public CatalogCategory() {
    }

    public CatalogCategory(Long id, String name, Long parent_id, Integer level) {
      this(id, name, parent_id, level, 1, 1);
    }

    public CatalogCategory(Long id, String name, Long parent_id, Integer level, Integer status, Integer type) {
      this.id = id;
      this.name = name;
      this.parent_id = parent_id;
      this.level = level;
      this.status = status;
      this.type = type;
    }

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Long getParent_id() {
      return parent_id;
    }

    public void setParent_id(Long parent_id) {
      this.parent_id = parent_id;
    }

    public Integer getLevel() {
      return level;
    }

    public void setLevel(Integer level) {
      this.level = level;
    }

    public Integer getStatus() {
      return status;
    }

    public void setStatus(Integer status) {
      this.status = status;
    }

    public Integer getType() {
      return type;
    }

    public void setType(Integer type) {
      this.type = type;
    }
  }

  public static class CatalogProduct {
    private Long id;
    private Long parent_id;
    private Long merchant_id;
    private Long category_id;
    private Integer status;
    private Integer custom_int_5;
    private Integer product_type;

    public CatalogProduct() {
    }

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public Long getParent_id() {
      return parent_id;
    }

    public void setParent_id(Long parent_id) {
      this.parent_id = parent_id;
    }

    public Long getMerchant_id() {
      return merchant_id;
    }

    public void setMerchant_id(Long merchant_id) {
      this.merchant_id = merchant_id;
    }

    public Long getCategory_id() {
      return category_id;
    }

    public void setCategory_id(Long category_id) {
      this.category_id = category_id;
    }

    public Integer getStatus() {
      return status;
    }

    public void setStatus(Integer status) {
      this.status = status;
    }

    public Integer getCustom_int_5() {
      return custom_int_5;
    }

    public void setCustom_int_5(Integer custom_int_5) {
      this.custom_int_5 = custom_int_5;
    }

    public Integer getProduct_type() {
      return product_type;
    }

    public void setProduct_type(Integer product_type) {
      this.product_type = product_type;
    }
  }
}
