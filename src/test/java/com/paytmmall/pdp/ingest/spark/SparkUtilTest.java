package com.paytmmall.pdp.ingest.spark;

import com.paytmmall.pdp.config.ApplicationConfig;
import com.paytmmall.pdp.config.Config;
import com.paytmmall.pdp.config.EnvironmentConfig;
import com.paytmmall.pdp.ingest.spark.SparkData.CatalogCategory;
import com.paytmmall.pdp.ingest.spark.SparkData.CatalogProduct;
import com.paytmmall.pdp.ingest.spark.SparkData.InventoryRecord;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class SparkUtilTest {

  @BeforeClass
  public static void loadConfig() {
    Constructor constructor = new Constructor(ApplicationConfig.class);
    Yaml yaml = new Yaml(constructor);
    InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("application.yml");
    ApplicationConfig applicationConfig = yaml.loadAs(inputStream, ApplicationConfig.class);
    final EnvironmentConfig environmentConfig = applicationConfig.getEnvironments().get("test");
    Config.setConfig(applicationConfig, environmentConfig);

    PropertyConfigurator.configure(
        Thread.currentThread().getContextClassLoader().getResourceAsStream("log4j.properties"));
  }

  @AfterClass
  public static void close() {
    SparkSessionFactory.getSparkSession().close();
  }

  @Test
  public void testInventorySum() {
    long product_id = 5l;
    List<InventoryRecord> inventoryData = SparkData
        .generateInventoryRecordForProduct(10, product_id);
    Dataset<Row> dataset = SparkSessionFactory.getSparkSession()
        .createDataset(inventoryData, Encoders.bean(InventoryRecord.class)).toDF();
    Dataset<Row> inventoryJoin = SparkUtil.buildInventoryJoin(dataset);
    List<Row> inventoryJoinResult = inventoryJoin.collectAsList();
    Assert.assertEquals(1, inventoryJoinResult.size());
    Assert.assertEquals(product_id, inventoryJoinResult.get(0).getLong(0));
    Assert.assertEquals(10, inventoryJoinResult.get(0).getLong(1));
  }

  @Test
  public void testCategoryFlatView() {
    List<CatalogCategory> categoryData = SparkData.generateLevelFiveCategory();
    Dataset<Row> dataset = SparkSessionFactory.getSparkSession()
        .createDataset(categoryData, Encoders.bean(CatalogCategory.class)).toDF();
    Dataset<Row> categoryJoin = SparkUtil.filterAndGenerateCategoryView(dataset);
    List<Row> categoryJoinResult = categoryJoin.collectAsList();
    Assert.assertEquals(categoryData.size(), categoryJoinResult.size());

    for (int i = 0; i < 5; i++) {
      verifyRowAtLevel(categoryJoinResult.get(i), i);
    }
  }

  @Test
  public void testCategoryFlatViewWithDisabledCategory() {
    List<CatalogCategory> categoryData = SparkData.generateLevelFiveCategory();
    categoryData.get(3).setStatus(0); //disable category 3. This should disable category 4
    Dataset<Row> dataset = SparkSessionFactory.getSparkSession()
        .createDataset(categoryData, Encoders.bean(CatalogCategory.class)).toDF();
    Dataset<Row> categoryJoin = SparkUtil.filterAndGenerateCategoryView(dataset);
    List<Row> categoryJoinResult = categoryJoin.collectAsList();
    Assert.assertEquals(categoryJoinResult.size(), 3);

    for (int i = 0; i < 3; i++) {
      verifyRowAtLevel(categoryJoinResult.get(i), i);
    }
  }

  @Test
  public void testProductCategoryInventoryJoin() {
    Random random = new Random();
    long product_id = random.nextInt(1000);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();

    CatalogProduct catalogProduct1 = SparkData.generateCatalogProductRecord(product_id, 2l);
    CatalogProduct catalogProduct2 = SparkData.generateCatalogProductRecord(product_id + 1, 3l);
    //product data set
    Dataset<Row> productDataset = sparkSession.createDataset(
        Arrays.asList(catalogProduct1, catalogProduct2),
        Encoders.bean(CatalogProduct.class)
    ).toDF();
    productDataset = SparkUtil.filterAndGenerateProductView(productDataset);
    //Inventory dataset
    Dataset<Row> inventoryDataset = sparkSession.createDataset(SparkData
        .generateInventoryRecordForProduct(10, product_id), Encoders.bean(InventoryRecord.class))
        .toDF();
    inventoryDataset = SparkUtil.buildInventoryJoin(inventoryDataset);

    //category dataset
    Dataset<Row> categoryDataset = sparkSession.createDataset(
        SparkData.generateLevelFiveCategory(),
        Encoders.bean(CatalogCategory.class)
    ).toDF();

    categoryDataset = SparkUtil.filterAndGenerateCategoryView(categoryDataset);
    Dataset<Row> resultDataset = SparkUtil
        .joinCatalogProductAndInventoryAndCategory(productDataset, inventoryDataset,
            categoryDataset);
    List<Row> result = resultDataset.collectAsList();
    Assert.assertEquals(2, result.size());
    Row row1 = result.get(0);
    Assert.assertEquals(row1.getLong(0), 2);
    Assert.assertEquals(row1.getLong(2), product_id);

    Row row2 = result.get(1);
    Assert.assertEquals(row2.getLong(0), 3);
    Assert.assertEquals(row2.getLong(2), product_id + 1);
  }

  @Test
  /*
  * In this test case, as the category_id is null, the join between product, inventory and category
  * should return no record.
  * */
  public void testProductCategoryInventoryJoinWhereCategoryIDIsNull() {
    Random random = new Random();
    long product_id = random.nextInt(1000);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();

    CatalogProduct catalogProduct1 = SparkData.generateCatalogProductRecord(product_id, null);
    //product data set
    Dataset<Row> productDataset = SparkUtil.filterAndGenerateProductView(sparkSession.createDataset(
        Arrays.asList(catalogProduct1),
        Encoders.bean(CatalogProduct.class)
    ).toDF());

    //Inventory dataset
    Dataset<Row> inventoryDataset = sparkSession.createDataset(SparkData
        .generateInventoryRecordForProduct(10, product_id), Encoders.bean(InventoryRecord.class))
        .toDF();
    inventoryDataset = SparkUtil.buildInventoryJoin(inventoryDataset);

    //category dataset
    Dataset<Row> categoryDataset = SparkUtil.filterAndGenerateCategoryView(sparkSession.createDataset(
        SparkData.generateLevelFiveCategory(),
        Encoders.bean(CatalogCategory.class)
    ).toDF());

    Dataset<Row> resultDataset = SparkUtil
        .joinCatalogProductAndInventoryAndCategory(productDataset, inventoryDataset,
            categoryDataset);
    List<Row> result = resultDataset.collectAsList();
    Assert.assertEquals(0, result.size());
  }

  @Test
  public void testProductCategoryInventoryJoinWhereCategoryIDInProductIsNotInCategory() {
    Random random = new Random();
    long product_id = random.nextInt(1000);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();

    CatalogProduct catalogProduct1 = SparkData.generateCatalogProductRecord(product_id, random.nextLong());
    //product data set
    Dataset<Row> productDataset = SparkUtil.filterAndGenerateProductView(sparkSession.createDataset(
        Arrays.asList(catalogProduct1),
        Encoders.bean(CatalogProduct.class)
    ).toDF());

    //Inventory dataset
    Dataset<Row> inventoryDataset = sparkSession.createDataset(SparkData
        .generateInventoryRecordForProduct(10, product_id), Encoders.bean(InventoryRecord.class))
        .toDF();
    inventoryDataset = SparkUtil.buildInventoryJoin(inventoryDataset);

    //category dataset
    Dataset<Row> categoryDataset = SparkUtil.filterAndGenerateCategoryView(sparkSession.createDataset(
        SparkData.generateLevelFiveCategory(),
        Encoders.bean(CatalogCategory.class)
    ).toDF());

    Dataset<Row> resultDataset = SparkUtil
        .joinCatalogProductAndInventoryAndCategory(productDataset, inventoryDataset,
            categoryDataset);
    List<Row> result = resultDataset.collectAsList();
    Assert.assertEquals(0, result.size());
  }

  private void verifyRowAtLevel(Row row, int level) {
    Assert.assertEquals(row.getLong(0), new Long(level).longValue()); //category_id
    switch (Integer.valueOf(level)) {
      case 0: {
        Assert.assertEquals(row.getString(1), "zero");
        Assert.assertEquals(row.getInt(2), 0);
        Assert.assertEquals(row.getLong(3), 0);
        Assert.assertEquals(row.getString(4), "zero");
        Assert.assertEquals(row.getInt(5), 0);
        Assert.assertEquals(row.getLong(6), 0);
        Assert.assertEquals(row.getString(7), "zero");
        Assert.assertEquals(row.getInt(8), 0);
        Assert.assertEquals(row.getLong(9), 0);
        Assert.assertEquals(row.getString(10), "zero");
        Assert.assertEquals(row.getInt(11), 0);

        Assert.assertEquals(row.getLong(12), 0);
        Assert.assertEquals(row.getString(13), "zero");
        break;
      }
      case 1: {
        Assert.assertEquals(row.getString(1), "one");
        Assert.assertEquals(row.getInt(2), 1);
        Assert.assertEquals(row.getLong(3), 0);
        Assert.assertEquals(row.getString(4), "zero");
        Assert.assertEquals(row.getInt(5), 0);
        Assert.assertEquals(row.getLong(6), 0);
        Assert.assertEquals(row.getString(7), "zero");
        Assert.assertEquals(row.getInt(8), 0);
        Assert.assertEquals(row.getLong(9), 0);
        Assert.assertEquals(row.getString(10), "zero");
        Assert.assertEquals(row.getInt(11), 0);
        Assert.assertEquals(row.getLong(12), 0);
        Assert.assertEquals(row.getString(13), "zero");
        break;
      }
      case 2: {
        Assert.assertEquals(row.getString(1), "two");
        Assert.assertEquals(row.getInt(2), 2);
        Assert.assertEquals(row.getLong(3), 1);
        Assert.assertEquals(row.getString(4), "one");
        Assert.assertEquals(row.getInt(5), 1);
        Assert.assertEquals(row.getLong(6), 0);
        Assert.assertEquals(row.getString(7), "zero");
        Assert.assertEquals(row.getInt(8), 0);
        Assert.assertEquals(row.getLong(9), 0);
        Assert.assertEquals(row.getString(10), "zero");
        Assert.assertEquals(row.getInt(11), 0);
        Assert.assertEquals(row.getLong(12), 0);
        Assert.assertEquals(row.getString(13), "zero");
        break;
      }
      case 3: {
        Assert.assertEquals(row.getString(1), "three");
        Assert.assertEquals(row.getInt(2), 3);
        Assert.assertEquals(row.getLong(3), 2);
        Assert.assertEquals(row.getString(4), "two");
        Assert.assertEquals(row.getInt(5), 2);
        Assert.assertEquals(row.getLong(6), 1);
        Assert.assertEquals(row.getString(7), "one");
        Assert.assertEquals(row.getInt(8), 1);
        Assert.assertEquals(row.getLong(9), 0);
        Assert.assertEquals(row.getString(10), "zero");
        Assert.assertEquals(row.getInt(11), 0);
        Assert.assertEquals(row.getLong(12), 0);
        Assert.assertEquals(row.getString(13), "zero");
        break;
      }
      case 4: {
        Assert.assertEquals(row.getString(1), "four");
        Assert.assertEquals(row.getInt(2), 4);
        Assert.assertEquals(row.getLong(3), 3);
        Assert.assertEquals(row.getString(4), "three");
        Assert.assertEquals(row.getInt(5), 3);
        Assert.assertEquals(row.getLong(6), 2);
        Assert.assertEquals(row.getString(7), "two");
        Assert.assertEquals(row.getInt(8), 2);
        Assert.assertEquals(row.getLong(9), 1);
        Assert.assertEquals(row.getString(10), "one");
        Assert.assertEquals(row.getInt(11), 1);
        break;
      }
      default: {
        throw new RuntimeException("Not an expeced level = " + level);
      }
    }
  }

  @Test
  public void filterCategoryViewWithType() {
    CatalogCategory zero = new CatalogCategory(0l, "zero", null, 0);
    CatalogCategory one = new CatalogCategory(1l, "one", 0l, 1);
    one.setType(2);
    Dataset<Row> dataset = SparkSessionFactory.getSparkSession()
        .createDataset(Arrays.asList(zero, one), Encoders.bean(CatalogCategory.class)).toDF();
    Dataset<Row> categoryJoin = SparkUtil.filterAndGenerateCategoryView(dataset);
    List<Row> categoryJoinList = categoryJoin.collectAsList();
    Assert.assertEquals(1, categoryJoinList.size());
    Assert.assertEquals(0, categoryJoinList.get(0).getLong(0));
  }

  @Test
  public void filterCategoryViewWithStatus() {
    CatalogCategory zero = new CatalogCategory(0l, "zero", null, 0);
    CatalogCategory one = new CatalogCategory(1l, "one", 0l, 1);
    one.setStatus(2);
    Dataset<Row> dataset = SparkSessionFactory.getSparkSession()
        .createDataset(Arrays.asList(zero, one), Encoders.bean(CatalogCategory.class)).toDF();
    Dataset<Row> categoryJoin = SparkUtil.filterAndGenerateCategoryView(dataset);
    List<Row> categoryJoinList = categoryJoin.collectAsList();
    Assert.assertEquals(1, categoryJoinList.size());
    Assert.assertEquals(0, categoryJoinList.get(0).getLong(0));
  }

  @Test
  public void filterProductViewWithStatus() {
    Random random = new Random();
    Long product_id = Long.valueOf(random.nextInt(100));
    Long category_id = Long.valueOf(random.nextInt(100));
    CatalogProduct cpr1 = SparkData.generateCatalogProductRecord(product_id, category_id);
    CatalogProduct cpr2 = SparkData.generateCatalogProductRecord(product_id + 1, category_id + 1);
    cpr2.setStatus(2);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();
    Dataset<Row> productDataset = sparkSession.createDataset(
        Arrays.asList(cpr1, cpr2),
        Encoders.bean(CatalogProduct.class)
    ).toDF();
    List<Row> filteredList = SparkUtil.filterAndGenerateProductView(productDataset).collectAsList();
    Assert.assertEquals(1, filteredList.size());
    Assert.assertEquals(category_id.longValue(), filteredList.get(0).getLong(0));
    Assert.assertEquals(product_id.longValue(), filteredList.get(0).getLong(2));
  }

  @Test
  public void filterProductViewWithCustomInt5() {
    Random random = new Random();
    Long product_id = Long.valueOf(random.nextInt(100));
    Long category_id = Long.valueOf(random.nextInt(100));
    CatalogProduct cpr1 = SparkData.generateCatalogProductRecord(product_id, category_id);
    CatalogProduct cpr2 = SparkData.generateCatalogProductRecord(product_id + 1, category_id + 1);
    cpr2.setCustom_int_5(2);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();
    Dataset<Row> productDataset = sparkSession.createDataset(
        Arrays.asList(cpr1, cpr2),
        Encoders.bean(CatalogProduct.class)
    ).toDF();
    List<Row> filteredList = SparkUtil.filterAndGenerateProductView(productDataset).collectAsList();
    Assert.assertEquals(1, filteredList.size());
    Assert.assertEquals(category_id.longValue(), filteredList.get(0).getLong(0));
    Assert.assertEquals(product_id.longValue(), filteredList.get(0).getLong(2));
  }

  @Test
  public void filterProductViewWithProductType() {
    Random random = new Random();
    Long product_id = Long.valueOf(random.nextInt(100));
    Long category_id = Long.valueOf(random.nextInt(100));
    CatalogProduct cpr1 = SparkData.generateCatalogProductRecord(product_id, category_id);
    CatalogProduct cpr2 = SparkData.generateCatalogProductRecord(product_id + 1, category_id + 1);
    cpr2.setProduct_type(2);
    final SparkSession sparkSession = SparkSessionFactory.getSparkSession();
    Dataset<Row> productDataset = sparkSession.createDataset(
        Arrays.asList(cpr1, cpr2),
        Encoders.bean(CatalogProduct.class)
    ).toDF();
    List<Row> filteredList = SparkUtil.filterAndGenerateProductView(productDataset).collectAsList();
    Assert.assertEquals(1, filteredList.size());
    Assert.assertEquals(category_id.longValue(), filteredList.get(0).getLong(0));
    Assert.assertEquals(product_id.longValue(), filteredList.get(0).getLong(2));
  }

}
