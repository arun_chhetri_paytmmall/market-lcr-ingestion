package com.paytmmall.pdp.ingest.spark;

import com.paytmmall.pdp.config.ApplicationConfig;
import com.paytmmall.pdp.config.Config;
import com.paytmmall.pdp.config.EnvironmentConfig;
import java.io.InputStream;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class DataUtilTest {

  @BeforeClass
  public static void loadConfig() {
    Constructor constructor = new Constructor(ApplicationConfig.class);
    Yaml yaml = new Yaml(constructor);
    InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("application.yml");
    ApplicationConfig applicationConfig = yaml.loadAs(inputStream, ApplicationConfig.class);
    final EnvironmentConfig environmentConfig = applicationConfig.getEnvironments().get("test");
    Config.setConfig(applicationConfig, environmentConfig);

    PropertyConfigurator.configure(
        Thread.currentThread().getContextClassLoader().getResourceAsStream("log4j.properties"));
  }

  @Test
  public void getOldDataPathTest() {
    final String path = DataUtil.getOldDataPath();
    System.out.println(path);
  }
}
