package com.paytmmall.pdp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class VerifyData {
  private static Logger LOGGER = Logger.getLogger(VerifyData.class);
  private static RestClient restClient;
  private static Connection conn;

  @BeforeClass
  public static void initDrivers() {
    PropertyConfigurator.configure(
        Thread.currentThread().getContextClassLoader().getResourceAsStream("log4j.properties"));

    HttpHost httpHost = HttpHost.create("http://localhost:9200");
    restClient = RestClient.builder(Arrays.asList(httpHost).toArray(new HttpHost[0])).build();
    final Properties dbProperties = new Properties();
    dbProperties.put("user", "root");
    dbProperties.put("password", "paytm@197");
    dbProperties.put("driver", "com.mysql.jdbc.Driver");
    try {
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mktplace?zeroDateTimeBehavior=convertToNull", dbProperties);
    } catch (Exception ex) {
      LOGGER.error(ex);
      throw new RuntimeException(ex);
    }
  }

  @Ignore("Only used to test in local dev")
  @Test
  public void matchDataInDBWithDataInElasticSearch() throws Exception {
    LOGGER.info("Matching data");
    try(Statement stmt = conn.createStatement()) {
      try (ResultSet rs = stmt.executeQuery("select id, category_id from catalog_product")) {
        long productCount = 0;
        while (rs.next()) {
          long id = rs.getLong(1);
          Long category_id = rs.getLong(2);
          try {
            Response response = restClient.performRequest("GET", "/product/product/" + Long.toString(id));
            if (response.getStatusLine().getStatusCode() != 200) {
              LOGGER.error("Product doesn't exists in es = " + Long.toString(id));
            }
          } catch (Exception ex) {
            if (category_id > 0) {
              LOGGER.error("Product doesn't exists in es = " + Long.toString(id));
            }
          }

          productCount++;
        }
        LOGGER.info("Downloaded " + Long.toString(productCount) + " products from mysql");
      }

    }
  }

}
