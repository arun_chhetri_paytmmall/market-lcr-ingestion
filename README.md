# README #

## How to build the project #
goto root of the project <br />
mvn package <br />
cd target <br />
pdp-ingest-<>-SNAPSHOT.jar is the jar which you can run on spark with the command below


```jshelllanguage
spark-submit --class "SlcrApplicationRunner" --master local[*] --driver-memory 10g --driver-class-path pdp-ingest-1.0-SNAPSHOT.jar --jars pdp-ingest-1.0-SNAPSHOT.jar pdp-ingest-1.0-SNAPSHOT.jar awsdevbox
```

### Curl to create product index in elastic search
``` javascript
curl -v -H "Content-type: application/json; charset=utf-8" http://sawslmktprodselplatsearch02:80/product -X PUT -d '{"settings":{"index.mapping.total_fields.limit": 20000}, "mappings": {"product": {"properties": {"date": {"type": "date","format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/ddepoch_millis"}}}}}'
curl -v -H "Content-type: application/json; charset=utf-8" http://localhost:9200/product -X PUT -d '{"settings":{"index.mapping.total_fields.limit": 20000}, "mappings": {"product": {"properties": {"date": {"type": "date","format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"}}}}}'


curl -v -H "Content-type: application/json; charset=utf-8" http://localhost:9200/product -X PUT -d '{
                                                                                                    	"settings": {
                                                                                                    		"index.mapping.total_fields.limit": 20000
                                                                                                    	},
                                                                                                    	"mappings": {
                                                                                                    		"doc": {
                                                                                                    			"properties": {
                                                                                                    				"attr_expiry_date": {
                                                                                                    					"type": "date",
                                                                                                    					"format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
                                                                                                    				},
                                                                                                    				"attr_manufacturing_date": {
                                                                                                    					"type": "date",
                                                                                                    					"format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
                                                                                                    				}
                                                                                                    			}
                                                                                                    		}
                                                                                                    	}
                                                                                                    }'
                                                                                                    
curl -v -H "Content-type: application/json; charset=utf-8" http://localhost:9200/product -X PUT -d '{"settings": {"index.mapping.total_fields.limit": 20000},"mappings": {"doc": {"properties": {"attr_expiry_date": {"type": "date","format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"},"attr_manufacturing_date": {"type": "date","format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"}}}}}'
                                                                                                    

```
